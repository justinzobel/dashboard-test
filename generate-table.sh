#!/bin/bash

# This creates a markdown table that you can place in a Gitlab README to show all project CI status in a group
# Note: Maximum is 100 projects due to Gitlab API limits, more could be done but is out of scope for my needs.
gitlaburl="https://invent.kde.org"
groupid="1547"
groupname="plasma-mobile"

if [[ -f /tmp/sortme ]];then rm /tmp/sortme;fi
if [[ -f /tmp/processme ]];then rm /tmp/processme;fi

# Get Project IDs
curl -s "${gitlaburl}/api/v4/groups/${groupid}/projects?per_page=100" | jq | grep "\"id\"" | grep -v ${groupid} | tr -s ' ' | cut -d " " -f 3 | cut -d , -f 1 > /tmp/gitlab1

# Get project name and default branch name:
for i in $(cat /tmp/gitlab1)
  do
    branch=$(curl -s ${gitlaburl}/api/v4/projects/${i}/repository/branches | jq -c '.[] | select(.default == true)' | jq | grep "\"name\"" | cut -d "\"" -f 4)
    name=$(curl -s ${gitlaburl}/api/v4/projects/${i} | jq | grep path_with_namespace | cut -d "\"" -f 4 | cut -d / -f 2)
    echo ${name},${branch} >> /tmp/sortme
done

cat /tmp/sortme | sort > /tmp/processme

echo "| Project | Branch | Status |"
echo "| ------ | ------ | ------ |"
for i in $(cat /tmp/processme);
  do
    name=$(echo ${i} | cut -d , -f 1)
    branch=$(echo ${i} | cut -d , -f 2)
    echo \| [${name}]\(${gitlaburl}/${groupname}/${name}/-/pipelines/latest\) \| ${branch} \| \![status]\(${gitlaburl}/${groupname}/${name}/badges/${branch}/pipeline.svg\) \|
done
